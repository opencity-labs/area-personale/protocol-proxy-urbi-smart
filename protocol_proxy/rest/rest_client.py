from __future__ import annotations
import requests
from typing import List
from settings import DEBUG, PROTOCOLL_SSL_VERIFY
from requests.auth import HTTPBasicAuth
from models.logger import get_logger
from models.protocollo import NewUser, Protocollo
from models.excepitions import UrbiSmartException, RestClientException

log = get_logger()

verify_ssl = bool(
    PROTOCOLL_SSL_VERIFY
)  # Assicurati che PROTOCOLL_SSL_VERIFY sia un booleano


class RestClient(object):
    def __init__(self, configuration: dict) -> dict:
        self.configuration = configuration

    def inserisci_protocollo(self, protocollo: Protocollo, attachments: List[str]):
        payload = protocollo.to_dict()
        try:
            log.debug("Inserisco protocollo")
            url = f"{self.configuration['api_url']}"
            params = {
                "WTDK_REQ": "insProtocollo",
                "WTDK_RESPONSE_FORMAT": "JSON",
            }
            auth = HTTPBasicAuth(
                self.configuration["username"], self.configuration["password"]
            )
            response = requests.post(
                url,
                params=params,
                auth=auth,
                data=payload,
                files=attachments,
                verify=verify_ssl,
            )

            log.debug(f"Risposta protocollo: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                log.error(
                    f"Error in post request, response: {response} - {response.content}",
                    exc_info=DEBUG,
                )
                raise RestClientException(
                    f"Error in post request, status code: {response.status_code}"
                )

        except UrbiSmartException as e:
            log.error(e, exc_info=DEBUG)
        except RestClientException as re:
            return False

    def get_user(self, fiscal_code: str):
        try:
            log.debug("Ottengo elenco corrispondenti")
            url = f"{self.configuration['api_url']}"
            params = {
                "WTDK_REQ": "getElencoCorrispondenti",
                "WTDK_RESPONSE_FORMAT": "JSON",
            }
            auth = HTTPBasicAuth(
                self.configuration["username"], self.configuration["password"]
            )
            response = requests.get(
                url,
                params=params,
                data={"PRCORE03_99991006_CodiceFiscale": fiscal_code},
                auth=auth,
                verify=verify_ssl,
            )

            log.debug(f"Risposta soggetto: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                log.error(
                    f"Error in get request, response: {response} - {response.content}",
                    exc_info=DEBUG,
                )
                raise RestClientException(
                    f"Error in get request, status code: {response.status_code}"
                )
        except UrbiSmartException as e:
            log.error(e, exc_info=DEBUG)
        except RestClientException as re:
            return False

    def create_user(self, user: NewUser):
        try:
            log.debug("Inserisco corrispondente")
            url = f"{self.configuration['api_url']}"
            params = {
                "WTDK_REQ": "insCorrispondente",
                "WTDK_RESPONSE_FORMAT": "JSON",
            }
            auth = HTTPBasicAuth(
                self.configuration["username"], self.configuration["password"]
            )
            response = requests.post(
                url,
                params=params,
                data=user.to_dict(),
                auth=auth,
                verify=verify_ssl,
            )

            log.debug(f"Risposta soggetto: {response}")
            if response.status_code == 200:
                return response.json()
            else:
                log.error(
                    f"Error in post request, response: {response} - {response.content}",
                    exc_info=DEBUG,
                )
                raise RestClientException(
                    f"Error in post request, status code: {response.status_code}"
                )
        except UrbiSmartException as e:
            log.error(e, exc_info=DEBUG)
        except RestClientException as re:
            return False
