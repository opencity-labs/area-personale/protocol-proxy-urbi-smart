import os
from __init__ import __version__ as app_version

VERSION = app_version
APP_NAME = "protocol-proxy-urbi-smart"
ENVIRONMENT = os.environ.get("ENVIRONMENT", "local")
DEBUG = os.environ.get("DEBUG", "true")
SERVER_ADDRESS_PORT = os.environ.get("SERVER_ADDRESS_PORT", "")
SENTRY_DSN = os.environ.get("SENTRY_DSN", "https://localhost:8080")
SENTRY_ENABLED = os.environ.get("SENTRY_ENABLED", "false").lower().strip() == "true"
SDC_AUTH_TOKEN_USER = os.environ.get("SDC_AUTH_TOKEN_USER", "admin")
SDC_AUTH_TOKEN_PASSWORD = os.environ.get("SDC_AUTH_TOKEN_PASSWORD", "admin")

# Kafka
KAFKA_BOOTSTRAP_SERVERS = os.environ.get("KAFKA_BOOTSTRAP_SERVERS", "kafka:9092")
KAFKA_CONSUMER_GROUP = os.environ.get("KAFKA_CONSUMER_GROUP", "documents")
KAFKA_CONSUMER_TOPIC = os.environ.get("KAFKA_CONSUMER_TOPIC", "documents")
KAFKA_PRODUCER_TOPIC = os.environ.get("KAFKA_PRODUCER_TOPIC", "documents")
KAFKA_RETRY_TOPIC = os.environ.get("KAFKA_RETRY_TOPIC", "retryqueue")
##Storage
STORAGE_ENDPOINT = os.environ.get("STORAGE_ENDPOINT", "http://minio:9000")
STORAGE_TYPE = os.environ.get("STORAGE_TYPE", "local")
STORAGE_BUCKET = os.environ.get("STORAGE_BUCKET", "protocollo")
# azure
STORAGE_BASE_PATH = os.environ.get("STORAGE_BASE_PATH", "data/")
STORAGE_AZURE_ACCOUNT = os.environ.get("STORAGE_AZURE_ACCOUNT", "admin")
STORAGE_AZURE_KEY = os.environ.get(
    "STORAGE_AZURE_KEY",
    "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq==",
)
# s3
STORAGE_ACCESS_S3_KEY = os.environ.get("STORAGE_ACCESS_S3_KEY", "Test_key_innonation")
STORAGE_KEY_S3_ACCESS_SECRET = os.environ.get(
    "STORAGE_KEY_S3_ACCESS_SECRET", "innonation"
)
STORAGE_S3_REGION = os.environ.get("STORAGE_S3_REGION", "us-east-1")
# local
STORAGE_LOCAL_PATH = os.environ.get(
    "STORAGE_LOCAL_PATH", "configuration/data"
)  # test api
PREFIX_API = os.environ.get("PREFIX_API", "/")
PROTOCOLL_SSL_VERIFY = (
    os.environ.get("PROTOCOLL_SSL_VERIFY", "false").lower().strip() == "true"
)
STAGE = os.environ.get("STAGE", "dev")
