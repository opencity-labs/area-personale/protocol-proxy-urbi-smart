from typing import Tuple
from models.types import FlowState
from settings import (
    KAFKA_BOOTSTRAP_SERVERS,
    KAFKA_CONSUMER_GROUP,
    KAFKA_CONSUMER_TOPIC,
    DEBUG,
)

from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def handle_exception(
    exception, message: dict, status: FlowState, attachments=None
) -> Tuple[dict, bool]:
    log.error(f"Errore durante il processamento: {exception}", exc_info=DEBUG)
    if "retry_meta" in message:
        message["retry_meta"]["original_topic"] = KAFKA_CONSUMER_TOPIC
    else:
        message["retry_meta"] = {
            "original_topic": KAFKA_CONSUMER_TOPIC,
        }
    if attachments:
        message["retry_meta"]["attachments"] = attachments
    message["status"] = status.value
    return message, False


def get_consumer_configuration() -> dict:
    return {
        "bootstrap.servers": KAFKA_BOOTSTRAP_SERVERS,
        "group.id": KAFKA_CONSUMER_GROUP,
        "auto.offset.reset": "earliest",
    }


def validate_message(message):
    return (
        "remote_id" in message
        and "id" in message
        and "remote_collection" in message
        and "tenant_id" in message
        and "event_version" in message
    )


def moving_message_in_process_pending(message: dict) -> dict:
    if "status" in message:
        message["retry"] = {
            "old_status": message["status"],
        }
    else:
        # caso vecchia tipologia di messaggio senza status
        message["retry"] = {
            "old_status": FlowState.REGISTRATION_PENDING.value,
        }
    message["status"] = FlowState.REGISTRATION_PENDING.value
    return message
