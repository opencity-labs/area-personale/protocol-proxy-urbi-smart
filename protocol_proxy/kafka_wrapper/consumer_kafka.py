from __future__ import annotations
import json
from typing import Union
from confluent_kafka import Consumer
from kafka_wrapper.producer_kafka import produce_message_after_consume
from models.excepitions import (
    MainDocumentException,
    ProtocolloException,
    JsonException,
)
from models.types import FlowState
from models.counter_metrics import CounterMetrics
from process.attachments import process_attachments_and_main_document
from process.protocollo import process_message_for_create_protocollo
from configuration.utils import check_configuration
from rest.rest_client import RestClient
from settings import (
    APP_NAME,
    KAFKA_CONSUMER_TOPIC,
    DEBUG,
    VERSION,
)
from kafka_wrapper.utils import (
    handle_exception,
    get_consumer_configuration,
    moving_message_in_process_pending,
    validate_message,
)
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def kafka_consumer(counter_metrics: CounterMetrics):
    log.debug("Start consume")
    # Configura il consumatore Kafka
    consumer_conf = get_consumer_configuration()
    # Crea un consumatore Kafka
    consumer = Consumer(consumer_conf)

    # Sottoscrivi il consumatore alla topic 'test_topic'
    consumer.subscribe([KAFKA_CONSUMER_TOPIC])
    while True:
        try:
            # Leggi i messaggi dalla topic
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            log.debug("Messaggio ricevuto: {}".format(msg.value().decode("utf-8")))
            # Verifica se il valore del messaggio è un JSON valido
            message = json.loads(msg.value().decode("utf-8"))
            if type(message) is not dict:
                raise JsonException()
            if msg.error():
                # Fine della partizione, continua la lettura
                log.error(
                    "Errore durante la lettura dei messaggi: {}".format(
                        msg.error().decode("utf-8")
                    ),
                    exc_info=DEBUG,
                )
                continue
            if not validate_message(message):
                log.error(
                    "Messaggio letto non contiene i campi desiderati: {}".format(
                        message
                    ),
                    exc_info=DEBUG,
                )
                continue
            if message["event_version"] != 1:
                log.error(
                    f"Messaggio letto contiene una versione errata: {message['event_version']}",
                    exc_info=DEBUG,
                )
                continue
            elif (
                "status" in message and message["status"]
                in [
                    FlowState.REGISTRATION_FAILED.value,
                    FlowState.PARTIAL_REGISTRATION.value,
                ]
                and message["app_id"].split(":")[0] == APP_NAME
            ):
                log.debug("Ignored message")
                continue
            else:
                log.info(
                    f"Messaggio letto con remote_id: {message['remote_id']} e id : {message['id']}"
                )
                # controllo servizio ok
                configuration = check_configuration(message["remote_collection"]["id"])
                if configuration is None or not configuration["is_active"]:
                    log.warning(
                        f'Errore durante la lettura della configurazione del servizio: {message["remote_collection"]["id"]}',
                        exc_info=DEBUG,
                    )
                    continue
                # controllo tenant ok
                if check_configuration(message["tenant_id"]) is None:
                    log.warning(
                        f'Errore durante la lettura della configurazione del tenant: {message["tenant_id"]}',
                        exc_info=DEBUG,
                    )
                    continue

                if (
                    "status" not in message
                    and message["registration_data"]["document_number"]
                ):
                    message["status"] = FlowState.REGISTRATION_COMPLETE.value

                # check registry_search_key se presente in caso inserisco
                if "registry_search_key" not in message:
                    message["registry_search_key"] = message["title"]

                # controllo se il messaggio è stato già protocollato
                if (
                    "status" in message
                    and message["status"] == FlowState.REGISTRATION_COMPLETE.value
                ):
                    log.info("Messaggio già protocollato")
                    continue

                message = fix_retry_too_large(message)

                if (
                    "status" in message
                    and message["status"] == FlowState.REGISTRATION_PENDING.value
                ):

                    # recupero vecchio stato nella retry
                    flow_message = FlowState(message["retry"]["old_status"])

                    # Configura il client REST
                    client = RestClient(configuration)

                    # processa il messaggio kafka
                    message, status = process_message_kafka(
                        message, configuration, client, flow_message
                    )
                    # completato processamento
                    if status:
                        message["status"] = FlowState.REGISTRATION_COMPLETE.value
                    produce_message_after_consume(message, status, counter_metrics)
                else:
                    # preparo messaggio per processamento
                    log.debug("Messaggio non ancora protocollato")
                    message = moving_message_in_process_pending(message)
                    produce_message_after_consume(message, True, counter_metrics)
        except JsonException as e:
            log.error("Il messaggio non è un JSON valido", exc_info=DEBUG)
            continue  # Salta al prossimo messaggio
        except KeyboardInterrupt:
            pass
        except Exception as e:
            log.error(f"Errore durante processamento {e}", exc_info=DEBUG)
            message, status = handle_exception(
                e, message, FlowState.REGISTRATION_FAILED
            )
            produce_message_after_consume(message, status, counter_metrics)


def process_message_kafka(
    message: dict, configuration: dict, client: RestClient, flow_message: FlowState
) -> Union[dict, bool]:
    processed_attachments: dict = {}
    try:
        processed_attachments: dict = process_attachments_and_main_document(message)
        log.info("Allegati caricati con successo")
        if processed_attachments is None:
            raise MainDocumentException()
        response, message = process_message_for_create_protocollo(
            message, processed_attachments, configuration, client
        )

        if not response:
            raise ProtocolloException()

        return message, True
    except MainDocumentException as mde:
        return handle_exception(mde, message, FlowState.REGISTRATION_FAILED)

    except ProtocolloException as pe:
        return handle_exception(pe, message, FlowState.PARTIAL_REGISTRATION)

    except Exception as e:
        flow = FlowState.REGISTRATION_FAILED
        return handle_exception(e, message, flow)


def fix_retry_too_large(message: dict):
    if (
        "retry_meta" in message
        and message["retry_meta"]
        and "attachments" in message["retry_meta"]
        and message["retry_meta"]["attachments"]
    ):
        message["retry_meta"]["attachments"] = None
    return message
