import asyncio
import json
from typing import Union

import sentry_sdk
from threading import Thread
from confluent_kafka.admin import AdminClient
from confluent_kafka import KafkaError
from fastapi.openapi.utils import get_openapi
from fastapi.responses import JSONResponse, PlainTextResponse
from fastapi import FastAPI, status
from prometheus_client import REGISTRY, generate_latest

from configuration.utils import (
    check_configuration,
    delete_configuration,
    read_local_schema,
    save_configuration,
)
from models.logger import get_logger
from models.counter_metrics import CounterMetrics
from models.api import (
    ConfigurationService,
    ConfigurationServicePatch,
    ConfigurationServicePut,
    ConfigurationTenantPatch,
    ConfigurationTenantPut,
    ErrorMessage,
    JSONResponseCustom,
    ConfigurationTenant,
    ERR_422_SCHEMA,
)
from kafka_wrapper.consumer_kafka import kafka_consumer
from models.excepitions import ExistException, NotExistingException
from models.types import MethodApi
from settings import (
    APP_NAME,
    ENVIRONMENT,
    KAFKA_CONSUMER_TOPIC,
    PREFIX_API,
    SENTRY_ENABLED,
    SENTRY_DSN,
    DEBUG,
    KAFKA_BOOTSTRAP_SERVERS,
    SERVER_ADDRESS_PORT,
)
from settings import VERSION as app_version
from storage.storage_manager import StorageManager

app = FastAPI(
    openapi_url=PREFIX_API + "openapi.json",
    docs_url=PREFIX_API + "docs",
    responses={422: ERR_422_SCHEMA},
)
storage_manager = StorageManager()

if SENTRY_ENABLED:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=1.0,
        # Set profiles_sample_rate to 1.0 to profile 100%
        # of sampled transactions.
        # We recommend adjusting this value in production.
        profiles_sample_rate=1.0,
        environment=ENVIRONMENT,
    )


# Ottieni il logger dalla configurazione centralizzata
log = get_logger()
counter_metrics = CounterMetrics()


# Endpoint per esporre le metriche in formato Prometheus
@app.get(
    "/metrics",
    response_class=PlainTextResponse,
    description="Metriche Prometheus",
    tags=["Metrics"],
)
def metrics():
    try:
        return generate_latest(REGISTRY)
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        log.error(
            f"An unexpected error occurred during the health check: {str(e)}",
            exc_info=DEBUG,
        )
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Health check failed",
            f"An unexpected error occurred during the health check: {str(e)}",
            MethodApi.GET,
        )


@app.get(
    "/status",
    tags=["Status"],
    description="Get Status",
    status_code=200,
    response_class=JSONResponseCustom,
    responses={
        503: {"model": ErrorMessage, "description": "Service Unavailable"},
        200: {"model": ErrorMessage, "description": "Successful Response"},
    },
)
def get_health_kafka_status():
    try:
        consumer_conf = {
            "bootstrap.servers": KAFKA_BOOTSTRAP_SERVERS,
        }

        # Crea un consumatore Kafka
        admin_client = AdminClient(consumer_conf)
        topics = admin_client.list_topics(timeout=5).topics
        log.debug(f"Topics: {topics}")

        if not topics:
            return _error_response(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                "No topics found",
                "No topics found. Health check failed",
                MethodApi.GET,
            )
        elif KAFKA_CONSUMER_TOPIC not in topics:
            return _error_response(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                "Topic not found",
                f"Expected topic '{KAFKA_CONSUMER_TOPIC}' not found. Health check failed",
                MethodApi.GET,
            )

        log.debug("Health check passed")
        status_code = status.HTTP_200_OK
        counter_metrics.increment_api_requests_total(status_code, MethodApi.GET)
        return JSONResponse(
            content=ErrorMessage(
                type=f"/no-error",
                title="status-ok",
                status=status_code,
                instance=APP_NAME,
                detail=str({"status": "ok"}),
            ).__dict__,
            status_code=status_code,
        )
    except KafkaError as ke:
        log.error(f"Health check failed: {ke}", exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Health check failed",
            f"Health check failed: {ke}",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )

    except Exception as e:
        log.error(
            f"An unexpected error occurred during the health check: {str(e)}",
            exc_info=DEBUG,
        )
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Health check failed",
            f"An unexpected error occurred during the health check: {str(e)}",
            MethodApi.GET,
        )


@app.get(
    "/schema",
    description="Get local Schema",
    tags=["Schema"],
)
def get_schema():
    try:
        data = read_local_schema()
        return JSONResponse(content=json.loads(data))
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.error(f"Error reading schema: {e}", exc_info=DEBUG)
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


# Endpoint to retrieve a tenant by ID
@app.get(
    "/tenants/{tenant_id}", description="Get Tenant Configuration", tags=["Tenants"]
)
async def get_tenant(tenant_id: str):
    try:
        tenants_data = check_configuration(tenant_id)
        # Check if the tenant_id exists in the data
        if tenants_data:
            return JSONResponse(content=tenants_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} is disabled or not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.post(
    "/tenants/",
    description="Create New Tenant Configuration",
    status_code=status.HTTP_201_CREATED,
    tags=["Tenants"],
)
async def save_tenant_configuration(config_tenant: ConfigurationTenant):
    try:
        if check_configuration(config_tenant.id):
            raise ExistException
        # Save the tenant to the local file
        tenant = config_tenant.to_dict()
        save_configuration(tenant)
        log.debug("Dati ricevuti con successo")

        # Return a response with status code 201 and no response body
        status_code = status.HTTP_201_CREATED
        counter_metrics.increment_api_requests_total(status_code, MethodApi.POST)
        return JSONResponse(
            status_code=status_code,
            content=tenant,
        )

    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.POST,
        )
    except ExistException as ee:
        # Log the error for debugging purposes
        log.error(f"Existing tenants: {config_tenant}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_409_CONFLICT,
            "Existing tenants",
            f"The tenants with id:{config_tenant.id} is already exist",
            MethodApi.POST,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Errore nella gestione della richiesta:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.POST,
        )


@app.put(
    "/tenants/{tenant_id}",
    description="Upddate Tenant Configuration",
    tags=["Tenants"],
)
async def update_tenant(tenant_id: str, updated_tenant: ConfigurationTenantPut):
    try:
        # Retrieve the tenant data from the local file
        tenants_data = check_configuration(tenant_id)
        # Check if the tenant_id exists in the data
        if tenants_data:
            # Update the tenant data with the new values
            tenants_data = updated_tenant.to_dict()
            # Save the updated tenant data to the local file
            tenants_data["id"] = tenant_id
            save_configuration(tenants_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code, MethodApi.PUT)
            return JSONResponse(status_code=status_code, content=tenants_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} not exist", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.PUT,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PUT,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PUT,
        )


# Endpoint to update a Tenants by ID with patch
@app.patch(
    "/tenants/{tenant_id}",
    description="Update Existing Tenants Configuration",
    tags=["Tenants"],
)
async def update_tenant_patch(tenant_id: str, updated_tenant: ConfigurationTenantPatch):
    try:
        tenants_data = check_configuration(tenant_id)
        # Check if the id exists in the data
        if tenants_data:
            update_dict = updated_tenant.to_dict()
            for key, value in update_dict.items():
                if key in tenants_data and value:
                    tenants_data[key] = value
                else:
                    log.debug(f"Key:{key} o value:{value} non presente")
            # Save the updated service data to the local file
            save_configuration(tenants_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code, MethodApi.PATCH)
            return JSONResponse(status_code=status_code, content=tenants_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} not exist", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.PATCH,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PATCH,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PATCH,
        )


# Endpoint to delete a tenant by ID
@app.delete(
    "/tenants/{tenant_id}",
    description="Delete Tenant",
    status_code=status.HTTP_204_NO_CONTENT,
    tags=["Tenants"],
)
async def delete_tenant(tenant_id: str):
    try:
        tenants_data = check_configuration(tenant_id)

        # Check if the tenant_id exists in the data
        if tenants_data:
            # Delete the tenant data
            delete_configuration(tenant_id)
            log.debug(f"Tenant {tenant_id}.json disabled successfully")
            status_code = status.HTTP_204_NO_CONTENT
            counter_metrics.increment_api_requests_total(status_code, MethodApi.DELETE)
            return JSONResponse(status_code=status_code, content={})
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} not exist or disabled", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.DELETE,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.DELETE,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.DELETE,
        )


# Endpoint to retrieve a services by ID
@app.get(
    "/services/{service_id}",
    description="Get Service Configuration",
    tags=["Services"],
)
async def get_service(service_id: str):
    try:
        service_data = check_configuration(service_id)
        # Check if the id exists in the data
        if service_data:
            counter_metrics.increment_api_requests_total(
                status.HTTP_200_OK, MethodApi.GET
            )
            return JSONResponse(content=service_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Service {service_id} is disabled or not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Service not found",
            "Service not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.post(
    "/services/",
    description="Save Service Configuration",
    status_code=status.HTTP_201_CREATED,
    tags=["Services"],
)
async def save_service_configuration(configuration_service: ConfigurationService):
    try:
        if check_configuration(configuration_service.id):
            raise ExistException
        service = configuration_service.to_dict()
        # Salvare i dati nei settings
        save_configuration(service)
        # Restituisci una risposta al client
        log.debug("Dati ricevuti con successo")
        status_code = status.HTTP_201_CREATED
        counter_metrics.increment_api_requests_total(status_code, MethodApi.POST)
        return JSONResponse(
            status_code=status_code,
            content=service,
        )

    except ExistException as ee:
        # Log the error for debugging purposes
        log.error(f"Existing services: {configuration_service}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_409_CONFLICT,
            "Existing services",
            f"The services with id:{configuration_service.id} is already exist",
            MethodApi.POST,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.POST,
        )
    except Exception as e:
        # Gestisci eventuali errori qui
        log.error("Errore nella gestione della richiesta:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.POST,
        )


# Endpoint to update a services by ID
@app.put(
    "/services/{service_id}",
    description="Update Service Configuration",
    tags=["Services"],
)
async def update_service(service_id: str, updated_service: ConfigurationServicePut):
    try:
        # Retrieve the service data from the local file
        services_data = check_configuration(service_id)
        # Check if the id exists in the data
        if services_data:
            tenant_id = services_data["tenant_id"]
            # Update the service data with the new values
            services_data = updated_service.to_dict()
            # Save the updated service data to the local file
            services_data["id"] = service_id
            services_data["tenant_id"] = tenant_id
            save_configuration(services_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code, MethodApi.PUT)
            return JSONResponse(status_code=status_code, content=services_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Service {service_id} not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Service not found",
            "Service not found",
            MethodApi.PUT,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PUT,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PUT,
        )


# Endpoint to update a services by ID with patch
@app.patch(
    "/services/{service_id}",
    description="Update Existing Service Configuration",
    tags=["Services"],
)
async def update_service_patch(
    service_id: str, updated_service: ConfigurationServicePatch
):
    try:
        # Retrieve the service data from the local file
        services_data = check_configuration(service_id)
        # Check if the id exists in the data
        if services_data:
            update_dict = updated_service.to_dict()
            for key, value in update_dict.items():
                if key in services_data and value:
                    services_data[key] = value
                else:
                    log.debug(f"Key:{key} o value:{value} non presente")
            save_configuration(services_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code, MethodApi.PATCH)
            return JSONResponse(status_code=status_code, content=services_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Service {service_id} not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Service not found",
            "Service not found",
            MethodApi.PATCH,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PATCH,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PATCH,
        )


# Endpoint to delete a service by ID
@app.delete(
    "/services/{service_id}",
    description="Delete Service",
    status_code=status.HTTP_204_NO_CONTENT,
    tags=["Services"],
)
async def delete_service(service_id: str):
    try:
        service_data = check_configuration(service_id)

        # Check if the id exists in the data
        if service_data:
            # Delete the service data
            delete_configuration(service_id)
            log.debug(f"Service {service_id}.json deleted successfully")
            status_code = status.HTTP_204_NO_CONTENT
            counter_metrics.increment_api_requests_total(status_code, MethodApi.DELETE)
            return JSONResponse(status_code=status_code, content={})
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Service {service_id} is disabled or not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Service not found",
            "Service not found",
            MethodApi.DELETE,
        )

    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.DELETE,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.DELETE,
        )


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    title = "Protocol Proxy Urbi-Smart"
    description = "Descrizione dell'API"
    terms_of_service = "https://example.com/terms"
    contact = {
        "name": "Opencity Labs Srl",
        "url": "https://opencitylabs.it/collabora-con-noi/",
        "email": "info@opencitylabs.it",
    }
    version = app_version
    openapi_tags = [
        {"name": "Metrics"},
        {"name": "Status"},
        {"name": "Schema"},
        {"name": "Tenants"},
        {"name": "Services"},
    ]
    servers = [
        {
            "url": SERVER_ADDRESS_PORT,
            "description": APP_NAME,
            "x-sandbox": True,
        }
    ]
    openapi_schema = get_openapi(
        title=title,
        version=version,
        tags=openapi_tags,
        servers=servers,
        description=description,
        terms_of_service=terms_of_service,
        contact=contact,
        routes=app.routes,
    )
    openapi_schema["info"]["x-api-id"] = APP_NAME
    openapi_schema["info"][
        "x-summary"
    ] = "Questo servizio si occupa di fare da intermediario tra OpenCity Italia - Area Personale e il sistema di protocollo Urbi-Smart"
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


def _error_response(
    status_code: status,
    title: str,
    detail: Union[str, dict],
    method: MethodApi = MethodApi.TRACE,
) -> JSONResponse:
    title_path = title.replace(" ", "-").lower()
    counter_metrics.increment_api_requests_total(status_code, method)

    # Costruisci il messaggio di errore
    error_message = ErrorMessage(
        type=f"/errors/{title_path}",
        title=title,
        status=status_code,  # Assicurati che status_code sia un intero
        detail=str(
            detail
        ),  # Assicurati di convertire il detail in stringa se è un dizionario
        instance=APP_NAME,  # Assicurati che APP_NAME sia definito e valido
    )

    return JSONResponse(content=error_message.to_dict(), status_code=status_code)


consumer_thread = Thread(target=kafka_consumer, args={counter_metrics})
consumer_thread.start()
