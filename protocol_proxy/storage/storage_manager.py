from models.types import StorageType
from storage.s3 import S3Storage
from storage.local import LocalStorage
from storage.azure import AzureStorage
from settings import DEBUG, STORAGE_TYPE, STORAGE_ENDPOINT
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()

storage_types = [member.value for member in StorageType]


class StorageManager:
    def __init__(self, storage_type: str = None, storage_endpoint: str = None):
        if STORAGE_TYPE not in storage_types:
            raise ValueError(f"Unsupported storage type: {STORAGE_TYPE}")
        if not storage_type:
            storage_type = STORAGE_TYPE
            storage_endpoint = STORAGE_ENDPOINT
        self.set_type_storage(storage_type, storage_endpoint)

    def set_type_storage(self, type_storage: str, storage_endpoint: str = None):
        self.storage_type = type_storage
        if self.storage_type == StorageType.AZURE.value and storage_endpoint:
            self.storage = AzureStorage(storage_endpoint)
        elif self.storage_type == StorageType.LOCAL.value:
            self.storage = LocalStorage()
        elif self.storage_type == StorageType.S3.value and storage_endpoint:
            self.storage = S3Storage(storage_endpoint)

    def read(self, key: str) -> str:
        try:
            return self.storage.read(key)
        except Exception as e:
            log.warning(f"Error reading from storage: {e}")

    def save(self, key: str, data: dict):
        try:
            return self.storage.save(key, data)
        except Exception as e:
            log.error(f"Error saving to storage: {e}", exc_info=DEBUG)

    def delete(self, key: str) -> bool:
        try:
            return self.storage.delete(key)
        except Exception as e:
            log.error(f"Error deleting from storage: {e}", exc_info=DEBUG)
