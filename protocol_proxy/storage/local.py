import json
import os
from models.logger import get_logger
from settings import STORAGE_LOCAL_PATH, DEBUG


# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


class LocalStorage:
    def __init__(self):
        self.local_path = STORAGE_LOCAL_PATH
        self.check_local_path()  # Verifica e crea il percorso locale all'inizializzazione

    def read(self, key: str) -> str:
        with open(f"{self.local_path}/{key}", "r") as file:
            return file.read()

    def save(self, key: str, data: dict):
        json_data = json.dumps(data)
        with open(f"{self.local_path}/{key}", "w") as file:
            file.write(json_data)
        log.debug("Stored configuration")
        return True

    def delete(self, key: str) -> bool:
        os.remove(f"{self.local_path}/{key}")
        return True

    def check_local_path(self) -> None:
        try:
            if not os.path.exists(self.local_path):
                os.makedirs(self.local_path)
            log.debug("Checked local path")
        except Exception as e:
            log.error(f"Error checking or creating local path: {e}", exc_info=DEBUG)
