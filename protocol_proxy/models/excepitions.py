class ProtocolloException(Exception):
    pass


class JsonException(Exception):
    pass


class ExistException(Exception):
    pass


class NotExistingException(Exception):
    pass


class UrbiSmartException(Exception):
    pass


class RestClientException(Exception):
    pass


class MainDocumentException(Exception):
    pass


class AttachmentsException(Exception):
    pass
