from prometheus_client import Counter
from models.types import MethodApi
from settings import ENVIRONMENT, APP_NAME

proxy = APP_NAME.replace("-", "_")

class CounterMetrics:
    def __init__(self) -> None:
        self.failed_events_counter = Counter(
            "document_failed_events_processed_total",
            "The total number of failed processed events",
            ["env", "tenant", "proxy"],
        )
        self.success_events_counter = Counter(
            "document_success_events_processed_total",
            "The total number of successfully processed events",
            ["env", "tenant", "proxy"],
        )
        self.api_requests_counter = Counter(
            "oc_api_requests_total",
            "The total number of api call events",
            ["env", "status_code", "proxy", "method"],
        )

    def increment_success(self, tenant_id: str):
        self.success_events_counter.labels(
            env=ENVIRONMENT, tenant=tenant_id, proxy=proxy
        ).inc()

    def increment_failure(self, tenant_id: str):
        self.failed_events_counter.labels(
            env=ENVIRONMENT, tenant=tenant_id, proxy=proxy
        ).inc()

    def increment_api_requests_total(self, status_code, method: MethodApi):
        self.api_requests_counter.labels(
            env=ENVIRONMENT, status_code=status_code, proxy=proxy, method=method.value
        ).inc()
