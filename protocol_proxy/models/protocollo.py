from __future__ import annotations
from typing import List
from models.types import TipoProtocollo, TipoVersoUfficio, RequestType, TypePerson


class NewUser:
    def __init__(self, user: dict):
        self.dict = {}
        if user["type"] == "human":
            self.dict["PRCORE03_99991007_TipoPersona"] = TypePerson.HUMAN.value
            self.dict["PRCORE03_99991007_CodiceFiscale"] = user[
                "tax_identification_number"
            ]
            self.dict["PRCORE03_99991007_Nome"] = user["name"]
            self.dict["PRCORE03_99991007_Cognome"] = user["family_name"]
        else:
            self.dict["PRCORE03_99991007_TipoPersona"] = TypePerson.LEGAL.value
            self.dict["PRCORE03_99991007_PartitaIVA"] = user[
                "tax_identification_number"
            ]
            self.dict["PRCORE03_99991007_Denominazione"] = user["name"]

    def to_dict(self) -> dict:
        return self.dict


class Protocollo:

    def __init__(
        self,
        configuration: dict,
        verso: TipoProtocollo,
        subject: str,
        num_attachments: int,
        user_code: List[str],
    ):
        self.idaoo: str = configuration["idaoo_code"]
        self.sezione: TipoProtocollo = verso
        self.oggetto: str = subject
        self.user_registrator: str = _extract_user_registrator(
            configuration["username"]
        )
        self.document_type: str = configuration["document_type"]
        self.destination_office_list: List[str] = configuration["office_code"]
        self.user_code: List[str] = user_code
        # si da per assodato che il numero non comprenda il documento principale
        self.num_attachments: str = str(num_attachments)
        if "id_fascicolo" in configuration:
            self.fascicolo: str = configuration["id_fascicolo"]
        else:
            self.fascicolo: str = None

    def to_dict(self) -> dict:
        """
        Converte l'istanza di Protocollo in formato JSON.
        """
        dict_protocollo = {
            "PRCORE03_99991009_IDAOO": self.idaoo,
            "PRCORE03_99991009_Sezione": self.sezione.value,
            "PRCORE03_99991009_Oggetto": self.oggetto,
            "PRCORE03_99991009_Utente_Registratore": self.user_registrator,
            "PRCORE03_99991009_Num_Allegati": self.num_attachments,
        }

        dict_protocollo["PRCORE03_99991009_Num_Corrispondenti"] = len(self.user_code)
        if self.document_type:
            dict_protocollo["PRCORE03_99991009_TipoDocumento"] = self.document_type
        for index, user in enumerate(self.user_code):
            dict_protocollo[
                f"PRCORE03_99991009_{index+1}_Corrispondente_CodiceSoggetto"
            ] = user

        if self.fascicolo:
            dict_protocollo["PRCORE03_99991009_IDFascicolo"] = self.fascicolo
        if self.sezione == TipoProtocollo.ARRIVO:
            dict_protocollo[
                f"PRCORE03_99991009_Num_Uffici_{TipoVersoUfficio.RECEIVER_S.value}"
            ] = len(self.destination_office_list)
            for index, office in enumerate(self.destination_office_list):
                dict_protocollo[
                    f"PRCORE03_99991009_{index+1}_Ufficio_{TipoVersoUfficio.RECEIVER.value}"
                ] = office
                dict_protocollo[
                    f"PRCORE03_99991009_{index+1}_Ufficio_{TipoVersoUfficio.RECEIVER.value}_Utenti_CO_Automatici"
                ] = RequestType.SUCCESS.value
        else:
            dict_protocollo[
                f"PRCORE03_99991009_Num_Uffici_{TipoVersoUfficio.SENDER_S.value}"
            ] = len(self.destination_office_list)
            for index, office in enumerate(self.destination_office_list):
                dict_protocollo[
                    f"PRCORE03_99991009_{index+1}_Ufficio_{TipoVersoUfficio.SENDER.value}"
                ] = office
                dict_protocollo[
                    f"PRCORE03_99991009_{index+1}_Ufficio_{TipoVersoUfficio.SENDER.value}_Utenti_CO_Automatici"
                ] = RequestType.SUCCESS.value

        return dict_protocollo


def _extract_user_registrator(user: str):
    # estrae user_registrator dalla stringa user è composta da user_registrator@codice
    user_registrator = user.split("@")[0]
    return user_registrator
