from typing import List, Optional, Union
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field


ERR_422_SCHEMA = {
    "description": "Validation Error",
    "content": {
        "application/problem+json": {
            "schema": {
                "$ref": "#/components/schemas/ErrorMessage",
            },
        },
    },
}


class InternalOffice(BaseModel):
    code: str
    type: str


class Status(BaseModel):
    status: str


class ErrorMessage(BaseModel):
    type: str
    title: str
    status: Optional[int] = Field(..., format="int32")
    detail: Union[str, dict]
    instance: Optional[str]

    def to_dict(self) -> dict:
        return self.dict()

class ConfigurationTenantPut(BaseModel):
    aoo_code: str
    base_url: str
    created_at: str = ""
    description: str
    institution_code: str
    modified_at: str = ""
    slug: str

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenant(ConfigurationTenantPut):
    id: str


class ConfigurationTenantPatch(ConfigurationTenantPut):
    aoo_code: str = ""
    base_url: str = ""
    created_at: str = ""
    description: str = ""
    institution_code: str = ""
    modified_at: str = ""
    slug: str = ""


class ConfigurationServicePut(BaseModel):
    is_active: bool
    username: str
    password: str
    office_code: List[str]
    api_url: str
    idaoo_code: str
    id_fascicolo: Optional[str]
    document_type: Optional[str]

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationServicePatch(ConfigurationServicePut):
    is_active: bool = False
    username: str = ""
    password: str = ""
    office_code: List[str] = []
    api_url: str = ""
    idaoo_code: str = ""
    id_fascicolo: str = ""
    document_type: str = ""


class ConfigurationService(ConfigurationServicePut):
    id: str
    tenant_id: str


class JSONResponseCustom(JSONResponse):
    media_type = "application/problem+json"
