import json

import requests
from settings import SDC_AUTH_TOKEN_PASSWORD, SDC_AUTH_TOKEN_USER, DEBUG
from models.logger import get_logger
from configuration.utils import check_configuration

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


class AuthClient:
    def __init__(self, tenant_id: str):
        configuration_tenant = check_configuration(tenant_id)
        self.base_url = configuration_tenant["base_url"]
        self.username = SDC_AUTH_TOKEN_USER
        self.password = SDC_AUTH_TOKEN_PASSWORD
        self._token = None

    @property
    def token(self):
        if self._token is None:
            self._token = self._get_auth_token()
        return self._token

    def _get_auth_token(self):
        url = f"{self.base_url}/auth"
        credentials = {"username": self.username, "password": self.password}
        log.debug(f"Invio richiesta POST a {url}")
        log.debug(f"Dati inviati: {credentials}")
        headers = {"Content-Type": "application/json"}
        json_data = json.dumps(credentials)
        try:
            response = requests.post(url, data=json_data, headers=headers)
            log.debug(f"Risposta POST: {response}")
            token = response.json().get("token")
            if token:
                log.debug(f"Token: {token}")
                return token
            else:
                log.error(f"Risposta JSON non valida: {response}", exc_info=DEBUG)
                raise ValueError("Token non presente nella risposta JSON")
        except requests.exceptions.RequestException as e:
            log.error(f"Errore durante la richiesta POST: {e}", exc_info=DEBUG)
            raise ValueError(f"Errore durante la richiesta POST: {e}")

    def download_file_with_auth(self, url):
        headers = {"Authorization": f"Bearer {self.token}"}
        try:
            response = requests.get(url, headers=headers)
            log.debug(f"Risposta GET: {response}")
            return response
        except requests.exceptions.RequestException as e:
            raise ValueError(f"Errore durante la richiesta GET autenticata: {e}")
