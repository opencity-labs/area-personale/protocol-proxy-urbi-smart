from enum import Enum


class FlowState(Enum):
    DOCUMENT_CREATED = "DOCUMENT_CREATED"
    REGISTRATION_PENDING = "REGISTRATION_PENDING"
    REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE"
    PARTIAL_REGISTRATION = "PARTIAL_REGISTRATION"
    REGISTRATION_FAILED = "REGISTRATION_FAILED"


class RequestType(Enum):
    SUCCESS = "S"
    ERROR = "N"


class TipoProtocollo(Enum):
    ARRIVO = "A"
    PARTENZA = "P"
    INTERNO = "I"


class TipoVersoUfficio(Enum):
    RECEIVER = "Destinatario"
    RECEIVER_S = "Destinatari"
    SENDER = "Mittente"
    SENDER_S = "Mittenti"


class TrasmissionType(Enum):
    """
    Enum Description:
    """

    INBOUND = "Inbound"
    OUTOUND = "Outbound"


class StorageType(Enum):
    AZURE = "azure"
    LOCAL = "local"
    S3 = "s3"


class MethodApi(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    PATCH = "PATCH"
    TRACE = "TRACE"


class TypePerson(Enum):
    LEGAL = "G"
    HUMAN = "F"
