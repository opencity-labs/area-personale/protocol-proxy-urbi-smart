from __future__ import annotations
from settings import DEBUG
from models.auth_token import AuthClient
from models.excepitions import (
    MainDocumentException,
    ProtocolloException,
    AttachmentsException,
)
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def process_attachments_and_main_document(message: dict) -> dict:
    processed_list_documents: dict = {}
    try:
        main_document = _download_pdf(
            message["main_document"]["url"], message["tenant_id"]
        )
        if not main_document:
            raise MainDocumentException()
        processed_list_documents[message["main_document"]["name"]] = (
            message["main_document"]["filename"],
            main_document,
            message["main_document"]["mime_type"],
        )
        # processa la lista degli allegati se non sono stati processati tutti
        processed_list_documents = _process_attachment_list(
            message, processed_list_documents
        )
        return processed_list_documents
    except MainDocumentException:
        log.error(
            f"Errore durante il caricamento del documento principale", exc_info=DEBUG
        )
        return None
    except ProtocolloException:
        log.error(f"Errore durante il processamento degli allegati", exc_info=DEBUG)
        return processed_list_documents
    except Exception as e:
        log.error(f"Errore durante il processamento dei documenti: {e}", exc_info=DEBUG)
        return processed_list_documents


def _process_attachment_list(message: dict, processed_list_documents: dict):
    for attachment in message["attachments"]:
        attachment_content = _download_pdf(attachment["url"], message["tenant_id"])
        if not attachment_content:
            raise AttachmentsException()
        processed_list_documents[attachment["name"]] = (
            attachment["filename"],
            attachment_content,
            attachment["mime_type"],
        )
    return processed_list_documents


def _download_pdf(url: str, tenant_id: str) -> str:

    auth_client = AuthClient(tenant_id)
    try:
        # Effettua una richiesta GET per scaricare il contenuto del PDF dalla URL
        log.debug(f"Richiesta GET per scaricare il contenuto del PDF dalla URL: {url}")
        response = auth_client.download_file_with_auth(url)
        # Verifica se la richiesta è stata eseguita con successo (codice 200)
        if response.status_code == 200:
            # Ottieni il contenuto del PDF
            return response.content
        else:
            # Se la richiesta non è andata a buon fine, stampa un messaggio di errore
            log.error(
                f"Errore nella richiesta. Codice: {response.status_code}",
                exc_info=DEBUG,
            )
            return None
    except Exception as e:
        # Se si verifica un errore durante la richiesta, stampa un messaggio di errore
        log.error(f"Errore durante la richiesta GET: {e}", exc_info=DEBUG)
        return None
