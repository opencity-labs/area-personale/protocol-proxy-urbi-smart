from __future__ import annotations
from datetime import datetime
from typing import List
from models.logger import get_logger
from models.protocollo import NewUser, Protocollo
from models.types import RequestType, TipoProtocollo, TrasmissionType
from models.excepitions import ProtocolloException
from rest.rest_client import RestClient

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def process_message_for_create_protocollo(
    message: dict,
    list_attachments: dict,
    configuration: dict,
    client: RestClient,
):
    transmission_type = message["registration_data"]["transmission_type"]
    response = process_message_protocollo(
        message,
        list_attachments,
        configuration,
        transmission_type,
        client,
    )
    log.debug(f"Processamento protocollo {response['result']['MESSAGE']}")
    message["registration_data"]["date"] = str(
        datetime.now().replace(microsecond=0).astimezone().isoformat()
    )
    message["registration_data"]["document_number"] = str(response["result"]["Numero"])
    message["folder"]["id"] = "n/a"
    log.info(f"Messaggio protocollato con successo {response}")
    return response, message


def process_message_protocollo(
    message: dict,
    attachments: dict,
    configuration: dict,
    transmission_type: str,
    client: RestClient,
) -> dict:
    log.debug("Inizio processamento protocollo")
    if transmission_type == TrasmissionType.INBOUND.value:
        tipo_protocollo = TipoProtocollo.ARRIVO
    else:
        tipo_protocollo = TipoProtocollo.PARTENZA
    # prendi il numero di documenti da inviare
    documents_number = len(message["attachments"])

    user_code_list = extract_user_registration_code(message["author"], client)

    # protocollo il messaggio
    protocollo = Protocollo(
        configuration,
        tipo_protocollo,
        message["folder"]["title"],
        documents_number,
        user_code_list,
    )

    response = client.inserisci_protocollo(protocollo, attachments)
    handle_exception(response, "Protocollo non inserito")
    return response


def extract_user_registration_code(users: List[dict], client: RestClient) -> List[str]:
    user_code_list = []
    for user in users:
        response_get = client.get_user(user["tax_identification_number"])
        handle_exception(response_get, "Elenco utenti non recuperato")
        if int(response_get["result"]["NumCorrispondenti"]) > 0:
            user_code_list.append(
                response_get["result"]["seq_Corrispondente"][0]["CodiceSoggetto"]
            )
            continue

        response_create = client.create_user(NewUser(user))
        handle_exception(response_create, "Utente non creato")
        user_code_list.append(response_create["result"]["CodiceSoggetto"])
    return user_code_list


def handle_exception(response, text: str):
    if not response:
        raise ProtocolloException(text)
    elif response["result"]["RESULT"] == RequestType.ERROR.value:
        raise ProtocolloException(
            f"{text} con errore: {response['result']['ERRORCODE']} {response['result']['MESSAGE']}"
        )
