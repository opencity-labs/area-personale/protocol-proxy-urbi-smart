import json
from unittest.mock import MagicMock, patch
import pytest

from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka import Producer, Consumer
from models.logger import get_logger
from process.attachments import process_attachments_and_main_document
from kafka_wrapper.consumer_kafka import process_message_kafka
from models.types import FlowState
from settings import STAGE
from rest.rest_client import RestClient

logging = get_logger()

# Definisci il nome del topic temporaneo per il test
TEMPORARY_TOPIC = "temporary_test_topic"

# Configurazione del broker Kafka
if STAGE == "local":
    KAFKA_BROKER = "localhost:29092"
else:
    KAFKA_BROKER = "kafka:9092"


with open("tests/configuration.json", "r") as file:
    configuration = json.load(file)

client = RestClient(configuration)
# Carica i messaggi di esempio
with open("tests/example_message_produced_in_kafka_sender.json", "r") as file:
    message_sender = json.load(file)

with open(
    "tests/example_message_produced_in_kafka_receiver.json",
    "r",
) as file:
    message_receiver = json.load(file)


@pytest.fixture(scope="function")
def test_kafka_consumer(request):
    # Crea un consumer per il test con un group.id univoco
    group_id = f"test_group_{request.node.name}"
    consumer_conf = {
        "bootstrap.servers": KAFKA_BROKER,
        "group.id": group_id,
        "auto.offset.reset": "earliest",
    }
    consumer = Consumer(consumer_conf)
    yield consumer
    # Chiudi il consumer dopo l'utilizzo
    consumer.close()


@pytest.fixture(scope="module")
def test_kafka_producer():
    # Crea un producer per il test
    producer_conf = {"bootstrap.servers": KAFKA_BROKER}
    producer = Producer(producer_conf)
    yield producer
    # Chiudi il producer dopo l'utilizzo
    producer.flush()


@pytest.fixture(scope="module")
def test_kafka_admin():
    # Crea un oggetto AdminClient per la gestione dei topic
    admin = AdminClient({"bootstrap.servers": KAFKA_BROKER})
    yield admin
    # Non è necessario chiudere l'oggetto AdminClient


@pytest.fixture(scope="function")
def test_kafka_topic(test_kafka_admin, request):
    # Crea un nome di topic univoco basato sul nome del test
    temporary_topic = f"{TEMPORARY_TOPIC}_{request.node.name}"
    # Crea il topic temporaneo per il test
    new_topic = NewTopic(temporary_topic, num_partitions=1, replication_factor=1)
    test_kafka_admin.create_topics([new_topic])
    yield temporary_topic
    # Cancella il topic dopo l'utilizzo
    test_kafka_admin.delete_topics([temporary_topic])


def test_kafka_message_flow_with_sender_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    logging.info("Sending message to Kafka...")
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_sender).encode("utf-8")
    )
    test_kafka_producer.flush()

    logging.info("Consuming message from Kafka...")
    test_kafka_consumer.subscribe([test_kafka_topic])
    try:
        msg = test_kafka_consumer.poll(timeout=20.0)
        if msg is None:
            raise Exception("No message received from Kafka.")
        received_message = msg.value().decode("utf-8")
        logging.debug(f"Received message: {received_message}")

        assert json.loads(received_message) == message_sender
    except Exception as e:
        logging.error(f"Error in message flow: {e}")

    # msg_send, status = process_message_kafka(
    #     message_sender, configuration, client, FlowState.DOCUMENT_CREATED
    # )
    # assert status == True

    # assert (
    #     msg_send["folder"]["id"] == "n/a"
    #     and msg_send["registration_data"]["date"]
    #     and msg_send["registration_data"]["document_number"]
    # )


def test_kafka_message_flow_with_receiver_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    logging.info("Sending message to Kafka...")
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_receiver).encode("utf-8")
    )
    test_kafka_producer.flush()

    logging.info("Consuming message from Kafka...")
    test_kafka_consumer.subscribe([test_kafka_topic])
    try:
        msg = test_kafka_consumer.poll(timeout=20.0)
        if msg is None:
            raise Exception("No message received from Kafka.")
        received_message = msg.value().decode("utf-8")
        logging.debug(f"Received message: {received_message}")

        assert json.loads(received_message) == message_receiver
    except Exception as e:
        logging.error(f"Error in message flow: {e}")

    # msg_send, status = process_message_kafka(message_receiver, configuration, client, FlowState.DOCUMENT_CREATED)
    # assert status == True

    # assert (
    #     msg_send["folder"]["id"] == "n/a"
    #     and msg_send["registration_data"]["date"]
    #     and msg_send["registration_data"]["document_number"]
    # )


def test_create_protocol_message_objects_with_sender_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    # Resetta la coda di retry prima del test
    _reset_retry_queue()

    # Invia un messaggio a Kafka
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_sender).encode("utf-8")
    )
    test_kafka_producer.flush()

    # Consuma il messaggio da Kafka
    test_kafka_consumer.subscribe([test_kafka_topic])
    msg = test_kafka_consumer.poll(timeout=20.0)
    assert msg is not None, "No message received for processing."

    received_message = json.loads(msg.value().decode("utf-8"))

    # Esegue il mock dei metodi necessari
    with patch("process.protocollo.process_message_for_create_protocollo"), patch(
        "process.attachments._download_pdf", return_value=b"mocked pdf content"
    ), patch(
        "process.attachments._process_attachment_list",
        return_value={
            "documento": (
                received_message["main_document"]["name"],
                b"mocked pdf content",
                "application/pdf",
            ),
            "allegato1": ("file1.pdf", b"mocked content", "application/pdf"),
        },
    ), patch(
        "rest.rest_client.RestClient.get_user",
        return_value={
            "error": "",
            "result": {
                "RESULT": "S",
                "NumCorrispondenti": "1",
                "seq_Corrispondente": [
                    {
                        "CodiceSoggetto": "5",
                        "TipoPersona": "F",
                        "Nome": "xxx",
                        "Cognome": "xxx",
                        "DataNascita": "00-00-0000",
                        "CodiceFiscale": "xxxx",
                        "__clz": "",
                    }
                ],
                "__clz": "",
            },
            "request_name": "getElencoCorrispondenti",
        },
    ), patch(
        "rest.rest_client.RestClient.inserisci_protocollo",
        return_value={
            "result": {
                "Anno": "2024",
                "DataProtocollo": "28-10-2024",
                "DenominazioneAOO": "AOO da definire",
                "IdAOO": "1",
                "IdProto": "22",
                "MESSAGE": "Inserito il protocollo correttamente.",
                "Numero": "22",
                "OraProtocollo": "11:21:51",
                "RESULT": "S",
            }
        },
    ):
        processed_attachments = process_attachments_and_main_document(received_message)
        message, success = process_message_kafka(
            received_message, configuration, client, FlowState.REGISTRATION_COMPLETE
        )

        # Verifica il corretto funzionamento
        assert (
            processed_attachments is not None
        ), "Processed attachments should not be None"
        assert "documento" in processed_attachments, "Main document should be processed"
        assert (
            processed_attachments["documento"][0]
            == received_message["main_document"]["name"]
        ), "Main document name should match"
        assert (
            processed_attachments["documento"][1] == b"mocked pdf content"
        ), "Main document content should be mocked"
        assert success is True, "The message processing should be successful"
        assert message is not None, "The processed message should not be None"


def test_create_protocol_message_objects_with_receiver_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    # Resetta la coda di retry prima del test
    _reset_retry_queue()

    # Invia un messaggio a Kafka
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_receiver).encode("utf-8")
    )
    test_kafka_producer.flush()

    # Consuma il messaggio da Kafka
    test_kafka_consumer.subscribe([test_kafka_topic])
    msg = test_kafka_consumer.poll(timeout=20.0)
    assert msg is not None, "No message received for processing."

    received_message = json.loads(msg.value().decode("utf-8"))

    # Esegue il mock dei metodi necessari
    with patch("process.protocollo.process_message_for_create_protocollo"), patch(
        "process.attachments._download_pdf", return_value=b"mocked pdf content"
    ), patch(
        "process.attachments._process_attachment_list",
        return_value={
            "documento": (
                received_message["main_document"]["name"],
                b"mocked pdf content",
                "application/pdf",
            ),
            "allegato1": ("file1.pdf", b"mocked content", "application/pdf"),
        },
    ), patch(
        "rest.rest_client.RestClient.get_user",
        return_value={
            "error": "",
            "result": {
                "RESULT": "S",
                "NumCorrispondenti": "1",
                "seq_Corrispondente": [
                    {
                        "CodiceSoggetto": "5",
                        "TipoPersona": "F",
                        "Nome": "xxx",
                        "Cognome": "xxx",
                        "DataNascita": "00-00-0000",
                        "CodiceFiscale": "xxxx",
                        "__clz": "",
                    }
                ],
                "__clz": "",
            },
            "request_name": "getElencoCorrispondenti",
        },
    ), patch(
        "rest.rest_client.RestClient.inserisci_protocollo",
        return_value={
            "result": {
                "Anno": "2024",
                "DataProtocollo": "28-10-2024",
                "DenominazioneAOO": "AOO da definire",
                "IdAOO": "1",
                "IdProto": "22",
                "MESSAGE": "Inserito il protocollo correttamente.",
                "Numero": "22",
                "OraProtocollo": "11:21:51",
                "RESULT": "S",
            }
        },
    ):

        # Esegue il test dei metodi principali
        processed_attachments = process_attachments_and_main_document(received_message)
        message, success = process_message_kafka(
            received_message, configuration, client, FlowState.REGISTRATION_COMPLETE
        )

        # Verifica il corretto funzionamento
        assert (
            processed_attachments is not None
        ), "Processed attachments should not be None"
        assert "documento" in processed_attachments, "Main document should be processed"
        assert (
            processed_attachments["documento"][0]
            == received_message["main_document"]["name"]
        ), "Main document name should match"
        assert (
            processed_attachments["documento"][1] == b"mocked pdf content"
        ), "Main document content should be mocked"
        assert success is True, "The message processing should be successful"
        assert message is not None, "The processed message should not be None"


def test_retry_queue_handling(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    # Resetta la coda di retry prima del test
    _reset_retry_queue()

    # Crea un messaggio senza allegati
    message_without_attachments = {
        "app_id": "document-dispatcher:1.3.16",
        "event_created_at": "2024-06-04T16:44:27+06:00",
        "main_document": {
            "url": "https://example.com/main_document.pdf",
            "filename": "main_document.pdf",
        },
        "attachments": [],
    }

    # Invia un messaggio a Kafka
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_without_attachments).encode("utf-8")
    )
    test_kafka_producer.flush()

    # Consuma il messaggio da Kafka
    test_kafka_consumer.subscribe([test_kafka_topic])
    msg = test_kafka_consumer.poll(timeout=20.0)
    assert msg is not None, "No message received for processing."

    received_message = json.loads(msg.value().decode("utf-8"))

    # Esegue il mock dei metodi necessari
    with patch(
        "process.attachments._download_pdf", return_value=b"mocked pdf content"
    ), patch("process.attachments._process_attachment_list", return_value={}):

        # Esegue il test del metodo principale
        processed_attachments = process_attachments_and_main_document(received_message)

        # Verifica il corretto funzionamento
        assert (
            processed_attachments is not None
        ), "Processed attachments should not be None"
        assert (
            processed_attachments == {}
        ), "Processed attachments should be an empty dictionary for messages without attachments"


def test_create_erroneous_kafka_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    # Resetta la coda di retry prima del test
    _reset_retry_queue()

    # Crea un messaggio errato
    erroneous_message = {
        "app_id": "document-dispatcher:1.3.16",
        "event_created_at": "2024-06-04T16:44:27+06:00",
        # "main_document"
        "attachments": [],
    }

    # Invia un messaggio a Kafka
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(erroneous_message).encode("utf-8")
    )
    test_kafka_producer.flush()

    # Consuma il messaggio da Kafka
    test_kafka_consumer.subscribe([test_kafka_topic])
    msg = test_kafka_consumer.poll(timeout=20.0)
    assert msg is not None, "No message received for processing."

    received_message = json.loads(msg.value().decode("utf-8"))

    # Esegue il mock dei metodi necessari
    with patch("process.protocollo.process_message_for_create_protocollo"), patch(
        "process.attachments._download_pdf"
    ), patch("process.attachments._process_attachment_list"), patch(
        "rest.rest_client.requests.post"
    ) as mock_requests_post:

        mock_response = MagicMock()
        mock_response.status_code = 400
        mock_response.json.return_value = {"error": "invalid message"}
        mock_requests_post.return_value = mock_response

        # Esegue il test dei metodi principali
        processed_attachments = process_attachments_and_main_document(received_message)
        message, success = process_message_kafka(
            received_message, configuration, client, FlowState.REGISTRATION_FAILED
        )

        # Verifica il corretto funzionamento
        assert (
            processed_attachments is not None
        ), "Processed attachments should not be None"
        assert (
            processed_attachments == {}
        ), "Processed attachments should be an empty dictionary for erroneous message"
        assert success is False, "The message processing should fail for invalid input"
        assert (
            "main_document" not in message
        ), "The message should not contain 'main_document'"


def _reset_retry_queue():
    global retry_queue
    retry_queue = {}
