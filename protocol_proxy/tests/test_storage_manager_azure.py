import json
from models.types import StorageType
from settings import STAGE
from storage.storage_manager import StorageManager

if STAGE == "local":
    HOST = "http://localhost:10000"
else:
    HOST = "http://azurite:10000"

storage_manager = StorageManager(StorageType.AZURE.value, HOST)
with open("tests/configuration.json", "r") as file:
    configuration = file.read()


def test_save_in_storage():
    assert storage_manager.save("test.json", configuration) is True


def test_read_in_storage():
    stored_data = storage_manager.read("test.json")
    if isinstance(stored_data, str):
        stored_data = json.loads(stored_data)
    assert stored_data == configuration


def test_delete_in_storage():
    assert storage_manager.delete("test.json") is True
