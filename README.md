# Protocol Proxy Urbi Smart

## Cosa fa

Questo servizio legge dal topic documents, e per ogni documento simulerà la relativa protocollazione.
Inoltre, il servizio espone le configurazione per i tenant, i servizi e lo schema utilizzati dalla stanza per abilitare tale proxy di protocollazione.

## Struttura del repository

### Configuration

Questo modulo serve a gestire le chiamate api di configurazione del servizio di protocollo.
Per testarlo bisogna andare `http://localhost:8060/docs` che punta al container di docker di fast-api.

### Kafka wrapper

Questo modulo serve a testare che si riesca a produrre e consumare un messaggio su kafka

### Rest

Questo modulo serve a testare le chiamate API REST che verrano impiegate dal protcollo

### Storage

Questo modulo contiene lo [StoreManager] che gestisce le chiamate di read and save file in abse alla configurazione nel `settings.py`
Lo storage va configurato nel file `.env` settando la variabile [STORAGE_TYPE] che può essere local, s3 o azure

### Process

Questo modulo contiene la gestione del processamento dell informazioni consumate da kafka, controllando le configurazioni, ed effettua le chiamate al sistema per protcollare il messaggio.

## Come si usa

1. Per installare il progetto è sufficiente creare un nuovo ambiente virtuale Python con il comando

```bash
python -m virtualenv venv
```

2. attivato l'ambiente virtuale, procedere con l'installazione delle dipendenze con il comando

```bash
pip install --upgrade -r .\requirements.txt
```

3. Assicuriamoci di avere installato sulla nostra macchina [Docker Desktop](https://www.docker.com/products/docker-desktop/), dopo avviamo un terminale e ci posizioniamo nella root del progetto, qui avvieremo il comando che creerà i volumi necessari al funzionamento del progetto

```bash
`docker-compose up -d`,
```

4. Assicuratevi che venga creato il topic 'documents', e poi mettere il nome del topic nel file `.env` e `settings.py`

5. Assicuratevi che venga creato il [BUCKET] che sia settata la [REGION] L'[ACCESS-KEY] e [SECRET-KEY]

6. Per lanciare il comando pytest eseguire prima `cd protocol_proxy` successivamente lanciare il comando `$env:STAGE='local'; $env:PYTHONPATH='.'; pytest --verbose --capture=tee-sys --disable-warnings --maxfail=1`

## Configurazione

Il sistema di configurazione prevede l'utilizzo del file `settings.py`
All'interno della directory principale esiste il file di esempio `.env.example` da poter usare
come traccia per la configurazione dell'ambiente.

### Configurazione variabili d'ambiente

| Nome                         | Default                                                          | Descrizione                                                                                                        |
| ---------------------------- | ---------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| ENVIRONMENT                  | local                                                            | Indica l'ambiente di sviluppo (locale, dev, prod, ecc.)                                                            |
| DEBUG                        | true                                                             | se impostato a true specifica il livello di log che viene stampato                                                 |
| SERVER_ADDRESS_PORT          | 8080                                                             | se impostato si può testare le API dallo swagger alla porta specificata                                            |
| KAFKA_CONSUMER_TOPIC         | documents                                                        | Topic di kafka da cui si leggono i documenti da protocollare                                                       |
| KAFKA_PRODUCER_TOPIC         | documents                                                        | Topic di kafka in cui si inviano i messaggio di fine protocollazione                                               |
| KAFKA_RETRY_TOPIC            | retryqueue                                                       | Topic di kafka in cui si inviano i messaggio che non sono stati protocollati                                       |
| KAFKA_BOOTSTRAP_SERVERS      | kafka:9092                                                       | Endpoint di kafka                                                                                                  |
| KAFKA_CONSUMER_GROUP         | documents                                                        | Nome del consumer group                                                                                            |
| STORAGE_TYPE                 | local                                                            | type of storage (local/s3/azure)                                                                                   |
| STORAGE_ACCESS_S3_KEY        | Test_key_innonation                                              | key di accesso a s3 storage                                                                                        |
| STORAGE_KEY_S3_ACCESS_SECRET | innonation                                                       | utente di accesso a s3 storage storage                                                                             |
| STORAGE_S3_REGION            | us-east-1                                                        | location del cloud storage                                                                                         |
| STORAGE_LOCAL_PATH           | configuration/data                                               | location della cartella locale storage                                                                             |
| STORAGE_BUCKET               | protocollo                                                       | Bucket di storage                                                                                                  |
| STORAGE_BASE_PATH            | data/                                                            | base path dello storage                                                                                            |
| STORAGE_AZURE_KEY            | Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq== | key di accesso ad azure                                                                                            |
| STORAGE_AZURE_ACCOUNT        | admin                                                            | utente di accesso ad azure                                                                                         |
| STORAGE_ENDPOINT             | http://azurite:10000                                             | Endpoint di azure                                                                                                  |
| PREFIX_API                   | /                                                                | prefisso per le Url delle API                                                                                      |
| SENTRY_ENABLED               | false                                                            | se impostato a true viene abilitato sentry                                                                         |
| SENTRY_DSN                   | https://<key>:<secret>@sentry.io/<project>                       | Endpoint per il monitoraggio di Sentry                                                                             |
| SDC_AUTH_TOKEN_USER          | admin                                                            | Utente per avere il token di autenticazione                                                                        |
| SDC_AUTH_TOKEN_PASSWORD      | admin                                                            | password per avere il token di autenticazione                                                                      |
| PROTOCOLL_SSL_VERIFY         | false                                                            | variabile per fare la request di inserimento protocollo con la verifica ssl o meno                                 |
| STAGE                        | dev/local                                                        | settare nel prorpio env con 'local' per poter eseguire il comando pytest sulla propria macchina se no lasciare dev |

### Configurazione Tenant

```json
{
  "id": "60e35f02-1509-408c-b101-3b1a28109329",
  "description": "Random Tenant Description",
  "slug": "random-tenant-slug",
  "base_url": "https://sdc.example.com",
  "institution_code": "random-institution-code",
  "aoo_code": "random-aoo-code",
  "created_at": "2023-06-05",
  "modified_at": "2023-08-07"
}
```

### Configurazione Servizio

```json
{
  "id": "configuration",
  "is_active": true,
  "username": "user@user",
  "idaoo_code": "x",
  "password": "xxx",
  "office_code": ["xxx", "yyy"],
  "id_fascicolo": "x",
  "document_type": "x",
  "api_url": "http://test.it",
  "tenant_id": "test-tenant"
}
```

## Swagger

le API sono documente e consultabili all'indirizzo:
base url + /docs

## Stadio di sviluppo

il software si trova in fase

## Autori e Riconoscimenti

## License

## Contatti
